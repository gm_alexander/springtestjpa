package com.example.controllers;

import com.example.entities.Book;
import com.example.entities.Tag;
import com.example.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@RestController
public class BookControllers {

    @Autowired
    private BookService service;

    @GetMapping(path = "/test", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getHello() {

        return ResponseEntity
                .ok()
                .body("This is work");
    }

    @GetMapping(path = "/book/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Book> getBook(@PathVariable("id") long id) {

        Optional<Book> optionalBook = service.getBook(id);
        if (optionalBook.isPresent()) {
            return ResponseEntity
                    .ok()
                    .body(optionalBook.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(path = "/book")
    public ResponseEntity<Book> saveBook(@RequestBody Book book) {
        Book resultBook = service.saveBook(book);
        return ResponseEntity
                .ok()
                .body(resultBook);
    }
}
