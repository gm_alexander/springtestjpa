package com.example.services;

import com.example.entities.Book;
import com.example.repositories.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;

@Component
public class BookService {

    @Autowired
    private BookRepository repository;
    @PersistenceContext
    EntityManager em;

    public Optional<Book> getBook(long id) {
       return repository.findById(id);
    }

    @Transactional
    public Book saveBook(Book book) {
        return em.merge(book);
    }
}
